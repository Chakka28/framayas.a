﻿using Framaya_APP.framayaEntities;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Framaya_APP.framaya.Reportes
{
    public partial class VerFactura : Form
    {
        public List<Factura> facturas = new List<Factura>();
        public VerFactura()
        {
            InitializeComponent();
        }

        private void VerFactura_Load(object sender, EventArgs e)
        {
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", facturas));
            this.reportViewer1.RefreshReport();
        }
    }
}
