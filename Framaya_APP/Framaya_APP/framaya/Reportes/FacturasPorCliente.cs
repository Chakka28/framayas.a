﻿using Framaya_APP.framayaBOL;
using Framaya_APP.framayaEntities;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Framaya_APP.framaya.Reportes
{
    public partial class FacturasPorCliente : Form
    {
        public Cliente Cliente { get; set; }
        FacturasBOL bol;

        public FacturasPorCliente()
        {
            InitializeComponent();
        }

        private void FacturasPorCliente_Load(object sender, EventArgs e)
        {
            bol = new FacturasBOL();
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", bol.facutaxCliente(Cliente.id.ToString())));
            this.reportViewer1.RefreshReport();
        }
    }
}
