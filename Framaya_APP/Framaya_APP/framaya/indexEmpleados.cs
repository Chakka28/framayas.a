﻿using Framaya_APP.framayaBOL;
using Framaya_APP.framayaEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Framaya_APP.framaya
{
    public partial class indexEmpleados : Form
    {
        private UsuarioBOL usuariobol;
        private RolBOL rolbol;
        private Usuario editrol;

        public indexEmpleados()
        {
            InitializeComponent();
        }

        private void MostrarDatos()
        {
            List<Usuario> p = usuariobol.CargarUsuarios("");
            dgEmpleados.DataSource = p;

        }

        private void MostrarRoles()
        {
            List<Rol> p = rolbol.CargarRoles("");
            comboBox1.DataSource = p;

        }

        private void indexEmpleados_Load(object sender, EventArgs e)
        {
            usuariobol = new UsuarioBOL();
            rolbol = new RolBOL();
            editrol = new Usuario();
            editrol.id = 0;
            MostrarDatos();
            MostrarRoles();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void dgEmpleados_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtcedula.Text.Trim()) || String.IsNullOrEmpty(txtNombre.Text.Trim())
                    || String.IsNullOrEmpty(txttelefono.Text.Trim()) || String.IsNullOrEmpty(txtdireccion.Text.Trim())
                    || String.IsNullOrEmpty(txtpass.Text.Trim()))
                {
                    notificacion.Text = "Datos vacios";
                    notificacion.Visible = true;

                }
                else
                {
                    editrol.nombre = txtNombre.Text.Trim();
                    editrol.cedula = txtcedula.Text.Trim();
                    editrol.direccion = txtdireccion.Text.Trim();
                    editrol.telefono = txttelefono.Text.Trim();
                    editrol.pass = txtpass.Text.Trim();
                    editrol.rolname = comboBox1.SelectedValue.ToString();
                    usuariobol.insetarUsurario(editrol);
                    MessageBox.Show("Datos agregados");
                    limpieza();
                    MostrarDatos();
                    editrol.id = 0;


                }
            }
            catch (Exception)
            {

                throw;
            }
        
    
    }
        

        public void limpieza()
        {
            txtNombre.Text = "";
            txtcedula.Text = "";
            txtdireccion.Text = "";
            txttelefono.Text = "";
            txtpass.Text = "";
        }

        private void dgEmpleados_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int row = dgEmpleados.SelectedRows.Count > 0 ? dgEmpleados.SelectedRows[0].Index : -1;
            if (row >= 0)
            {

                editrol = (Usuario)dgEmpleados.CurrentRow.DataBoundItem;
                MessageBox.Show("Has seleccionado la columna de: " + editrol.nombre);

            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (editrol.id > 0)
            {
                txtNombre.Text = editrol.nombre;
                txtcedula.Text = editrol.cedula;
                txtpass.Text = editrol.pass;
                txtdireccion.Text = editrol.direccion;
                txttelefono.Text = editrol.telefono;
                comboBox1.SelectedValue = editrol.rol.id;

            }
            else
            {
                MessageBox.Show("Debe seleccionar un empleado antes de modificarlo");
            }

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (editrol.id > 0)
            {
                DialogResult oDlgRes;

                oDlgRes = MessageBox.Show("¿Está seguro que desea eliminar" +
                    " el registro seleccionado ?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                if (oDlgRes == DialogResult.Yes)
                {
                    usuariobol.Eliminar(editrol);
                    editrol.id = 0;
                    MessageBox.Show("Columna eliminada");
                    MostrarDatos();
                }
                else
                {
                    MessageBox.Show("No se elimino la columna");
                }


            }
            else
            {
                MessageBox.Show("Debe seleccionar un empleado antes de Eliminarlo");
            }

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            List<Usuario> p = usuariobol.CargarUsuarios(txtBuscar.Text.Trim());
            dgEmpleados.DataSource = p;
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
