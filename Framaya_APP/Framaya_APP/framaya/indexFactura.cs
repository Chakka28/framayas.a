﻿using Framaya_APP.framaya.Reportes;
using Framaya_APP.framayaBOL;
using Framaya_APP.framayaDAL;
using Framaya_APP.framayaEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Framaya_APP.framaya
{
    public partial class indexFactura : Form
    {
        public Usuario Usuario { get; set; }
        private FacturasBOL bol;
        public Factura factura;
        public indexFactura()
        {
            InitializeComponent();
        }

        private void indexFactura_Load(object sender, EventArgs e)
        {
            factura = new Factura();
            factura.id = 0;
            bol = new FacturasBOL();
            MostrarDatos();
        }

        private void MostrarDatos()
        {
            List<Factura> p = bol.Cargar("");
            dgFacturas.DataSource = p;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            NuevaFactura nf = new NuevaFactura
            {
                Usuario = Usuario
            };
            nf.Show();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            List<Factura> p = bol.Cargar(txtBuscar.Text.Trim());
            dgFacturas.DataSource = p;
        }

        private void txtVerFactura_Click(object sender, EventArgs e)
        {
            if (factura.id > 0)
            {

                VerFactura mp = new VerFactura();
                mp.facturas.Add(factura);
                mp.Show(); 
            }
            else
            {
                MessageBox.Show("No has seleccionado una factura");
            }
        }

        private void dgFacturas_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int row = dgFacturas.SelectedRows.Count > 0 ? dgFacturas.SelectedRows[0].Index : -1;
            if (row >= 0)
            {

                factura = (Factura)dgFacturas.CurrentRow.DataBoundItem;
                MessageBox.Show("Has seleccionado la columna: " + factura.id);

            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (factura.id > 0)
            {
                DialogResult oDlgRes;

                oDlgRes = MessageBox.Show("¿Está seguro que desea eliminar" +
                    " el registro seleccionado ?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                if (oDlgRes == DialogResult.Yes)
                {
                    bol.Eliminar(factura);
                    factura.id = 0;
                    MessageBox.Show("Columna eliminada");
                    MostrarDatos();
                }
                else
                {
                    MessageBox.Show("No se elimino la columna");
                }


            }
            else
            {
                MessageBox.Show("Debe seleccionar un factura antes de Eliminarlo");
            }
        }
    }
}
