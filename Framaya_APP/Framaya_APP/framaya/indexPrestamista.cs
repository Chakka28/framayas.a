﻿using Framaya_APP.framayaBOL;
using Framaya_APP.framayaEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Framaya_APP.framaya
{
    public partial class indexPrestamista : Form
    {
        private PrestamistaBOL Prestamistabol;
        private Prestamista clienteEdit;

        public indexPrestamista()
        {
            InitializeComponent();
        }

        private void indexPrestamista_Load(object sender, EventArgs e)
        {
            Prestamistabol = new PrestamistaBOL();
            clienteEdit = new Prestamista();
            clienteEdit.id = 0;
            MostrarDatos();
        }

        private void MostrarDatos()
        {
            List<Prestamista> p = Prestamistabol.Cargarpretamistas("");
            dgPrestamistas.DataSource = p;

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtEmail.Text.Trim()) || String.IsNullOrEmpty(txtNombre.Text.Trim())
                    || String.IsNullOrEmpty(txttelefono.Text.Trim()) || String.IsNullOrEmpty(txtdireccion.Text.Trim()))
                {
                    notificacion.Text = "Datos vacios";
                    notificacion.Visible = true;

                }
                else
                {
                    clienteEdit.nombre = txtNombre.Text.Trim();
                    clienteEdit.email = txtEmail.Text.Trim();
                    clienteEdit.direccion = txtdireccion.Text.Trim();
                    clienteEdit.telefono = txttelefono.Text.Trim();
                    Prestamistabol.insetarPrestamista(clienteEdit);
                    MessageBox.Show("Datos agregados");
                    limpieza();
                    MostrarDatos();
                    clienteEdit.id = 0;


                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void limpieza()
        {
            txtNombre.Text = "";
            txtEmail.Text = "";
            txtdireccion.Text = "";
            txttelefono.Text = "";
        }

        private void dgPrestamistas_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int row = dgPrestamistas.SelectedRows.Count > 0 ? dgPrestamistas.SelectedRows[0].Index : -1;
            if (row >= 0)
            {

                clienteEdit = (Prestamista)dgPrestamistas.CurrentRow.DataBoundItem;
                MessageBox.Show("Has seleccionado la columna de: " + clienteEdit.nombre);

            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (clienteEdit.id > 0)
            {
                txtNombre.Text = clienteEdit.nombre;
                txtEmail.Text = clienteEdit.email;
                txtdireccion.Text = clienteEdit.direccion;
                txttelefono.Text = clienteEdit.telefono;

            }
            else
            {
                MessageBox.Show("Debe seleccionar un Prestamista antes de modificarlo");
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (clienteEdit.id > 0)
            {
                DialogResult oDlgRes;

                oDlgRes = MessageBox.Show("¿Está seguro que desea eliminar" +
                    " el registro seleccionado ?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                if (oDlgRes == DialogResult.Yes)
                {
                    Prestamistabol.Eliminar(clienteEdit);
                    clienteEdit.id = 0;
                    MessageBox.Show("Columna eliminada");
                    MostrarDatos();
                }
                else
                {
                    MessageBox.Show("No se elimino la columna");
                }


            }
            else
            {
                MessageBox.Show("Debe seleccionar un prestamista antes de Eliminarlo");
            }
        }
    }
}
