﻿using Framaya_APP.framayaBOL;
using Framaya_APP.framayaEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Framaya_APP.framaya
{
    public partial class indexEmpresa : Form
    {
        private EmpresaBOL bol;
        private Empresa Edit;
        public indexEmpresa()
        {
            InitializeComponent();
        }

        private void indexEmpresa_Load(object sender, EventArgs e)
        {
            bol = new EmpresaBOL();
            Edit = new Empresa();
            Edit.id = 0;
            MostrarDatos();
        }
        private void MostrarDatos()
        {
            List<Empresa> p = bol.Cargar("");
            dgEmpresa.DataSource = p;

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtEmail.Text.Trim()) || String.IsNullOrEmpty(txtNombre.Text.Trim())
                    || String.IsNullOrEmpty(txttelefono.Text.Trim()) || String.IsNullOrEmpty(txtdireccion.Text.Trim())
                    || String.IsNullOrEmpty(txtDesp.Text.Trim()))
                {
                    notificacion.Text = "Datos vacios";
                    notificacion.Visible = true;

                }
                else
                {
                    Edit.nombre = txtNombre.Text.Trim();
                    Edit.Tipo_Empresa = txtDesp.Text.Trim();
                    Edit.email = txtEmail.Text.Trim();
                    Edit.direccion = txtdireccion.Text.Trim();
                    Edit.telefono = txttelefono.Text.Trim();
                    bol.insetar(Edit);
                    MessageBox.Show("Datos agregados");
                    limpieza();
                    MostrarDatos();
                    Edit.id = 0;


                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public void limpieza()
        {
            txtDesp.Text = "";
            txtNombre.Text = "";
            txtEmail.Text = "";
            txtdireccion.Text = "";
            txttelefono.Text = "";
        }

        private void dgEmpresa_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int row = dgEmpresa.SelectedRows.Count > 0 ? dgEmpresa.SelectedRows[0].Index : -1;
            if (row >= 0)
            {

                Edit = (Empresa)dgEmpresa.CurrentRow.DataBoundItem;
                MessageBox.Show("Has seleccionado la columna de: " + Edit.nombre);

            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (Edit.id > 0)
            {
                txtNombre.Text = Edit.nombre;
                txtEmail.Text = Edit.email;
                txtdireccion.Text = Edit.direccion;
                txttelefono.Text = Edit.telefono;
                txtDesp.Text = Edit.Tipo_Empresa;

            }
            else
            {
                MessageBox.Show("Debe seleccionar un empresa antes de modificarlo");
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (Edit.id > 0)
            {
                DialogResult oDlgRes;

                oDlgRes = MessageBox.Show("¿Está seguro que desea eliminar" +
                    " el registro seleccionado ?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                if (oDlgRes == DialogResult.Yes)
                {
                    bol.Eliminar(Edit);
                    Edit.id = 0;
                    MessageBox.Show("Columna eliminada");
                    MostrarDatos();
                }
                else
                {
                    MessageBox.Show("No se elimino la columna");
                }


            }
            else
            {
                MessageBox.Show("Debe seleccionar un empresa antes de Eliminarlo");
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {

        }
    }
}
