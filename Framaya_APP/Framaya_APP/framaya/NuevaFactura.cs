﻿using Framaya_APP.framayaBOL;
using Framaya_APP.framayaEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Framaya_APP.framaya
{
    public partial class NuevaFactura : Form
    {
        private EmpresaBOL empresabol;
        private ClienteBOL clientebol;
        private PrestamistaBOL prestamistabol;
        private FacturasBOL bol;
        public Usuario Usuario { get; set; }

        public NuevaFactura()
        {
            InitializeComponent();
        }

        private void NuevaFactura_Load(object sender, EventArgs e)
        {
            empresabol = new EmpresaBOL();
            clientebol = new ClienteBOL();
            prestamistabol = new PrestamistaBOL();
            bol = new FacturasBOL();
            notificacion.Visible = false;
            Mostrardatoos();
        }

        private void Mostrardatoos()
        {
            List<Empresa> p = empresabol.Cargar("");
            cbEmpresa.DataSource = p;
            List<Cliente> e = clientebol.CargarClientes("");
            cbCliente.DataSource = e;
            List<Prestamista> q = prestamistabol.Cargarpretamistas("");
            cbPrestamista.DataSource = q;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtMonto.Text.Trim()))
                {
                    notificacion.Text = "Datos vacios";
                    notificacion.Visible = true;

                }
                else
                {
                    Factura Edit = new Factura();
                    Edit.NombreUsuario = Usuario.id.ToString();
                    Edit.NombreCliente = cbCliente.SelectedValue.ToString();
                    Edit.NombreEmpresa = cbEmpresa.SelectedValue.ToString();
                    Edit.NombrePrestamista = cbPrestamista.SelectedValue.ToString();
                    Edit.Fecha = dateTimePicker1.Value;
                    Edit.estado = "Pendiente";
                    Edit.monto = Convert.ToInt32(txtMonto.Text.Trim());
                    bol.insetar(Edit);
                    MessageBox.Show("Datos agregados");
                    this.Close();


                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
