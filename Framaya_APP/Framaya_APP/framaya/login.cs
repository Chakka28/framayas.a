﻿using Framaya_APP.framayaBOL;
using Framaya_APP.framayaEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Framaya_APP.framaya
{
    public partial class login : Form
    {
        private loginBOL loginBOL;
        public login()
        {
            InitializeComponent();
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void login_Load(object sender, EventArgs e)
        {
            FormBorderStyle = FormBorderStyle.FixedDialog;
            error.Visible = false;
            loginBOL = new loginBOL();
        }

        private void btnlogin_Click(object sender, EventArgs e)
        {
            try
            {
                if(txtpassword.Text != "" && txtusuario.Text.Trim() != "")
                {
                    Usuario usuario = new Usuario
                    {
                        cedula = txtusuario.Text.Trim(),
                        pass = txtpassword.Text.Trim(),

                    };
                    usuario = loginBOL.Login(usuario);
                    if(usuario.id != 0)
                    {
                        inicio mp = new inicio
                        {
                            Usuario = usuario
                        };
                        mp.Show(this);
                        Hide();

                    }
                    else
                    {
                        error.Text = "Usuario no existe";
                        error.Visible = true;
                    }
                }
                else
                {
                    error.Text = "No se han terminado de llenar los datos";
                    error.Visible = true;
                }
                

            }
            catch (Exception)
            {

            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
