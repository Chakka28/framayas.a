﻿
using Framaya_APP.framayaEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Framaya_APP.framaya
{
    public partial class inicio : Form
    {
        public Usuario Usuario { get; set; }

        public inicio()
        {
            InitializeComponent();
        }

        private void panelContenedor_Paint(object sender, PaintEventArgs e)
        {

        }
        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]

        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }



        private void BarraTitulo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnReportes_Click_1(object sender, EventArgs e)
        {
            
        }

        private void inicio_Load(object sender, EventArgs e)
        {
            if(Usuario.rol.nombre.ToString() == "Administrador")
            {
                btnempleados.Visible = true;
                btnroles.Visible = true;
            }
            else
            {
                btnempleados.Visible = false;
                btnroles.Visible = false;
            }

            AbrirFormEnPanel(new principal(){
                Usuario = Usuario
            });
        }

        private void button6_Click(object sender, EventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void inicio_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Desea salir del sistema?",
                 "Mensaje del sistema", MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes)
            {
                if (Owner != null)
                {
                    Owner.Show();
                }

            }
            else
            {
                e.Cancel = true;
            }

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new principal()
            {
                Usuario = Usuario
            });
        }

        private void AbrirFormEnPanel(object formhija)
        {
            if (this.panelContenedor.Controls.Count > 0)
                this.panelContenedor.Controls.RemoveAt(0);
            Form fh = formhija as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.panelContenedor.Controls.Add(fh);
            this.panelContenedor.Tag = fh;
            fh.Show();

        }

        private void btnroles_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new indexRoles());
        }

        private void btnempleados_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new indexEmpleados());
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new indexCliente());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new indexPrestamista());
        }

        private void btnEmpresas_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new indexEmpresa());
        }

        private void btnfacturas_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new indexFactura()
            {
                Usuario = Usuario
            });

        }
    }
    }