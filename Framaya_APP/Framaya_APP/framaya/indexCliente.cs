﻿using Framaya_APP.framaya.Reportes;
using Framaya_APP.framayaBOL;
using Framaya_APP.framayaEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Framaya_APP.framaya
{
    public partial class indexCliente : Form
    {
        private ClienteBOL Clientebol;
        private Cliente clienteEdit;

        public indexCliente()
        {
            InitializeComponent();
        }

        private void MostrarDatos()
        {
            List<Cliente> p = Clientebol.CargarClientes("");
            dgClientes.DataSource = p;

        }

        private void indexCliente_Load(object sender, EventArgs e)
        {
            Clientebol = new ClienteBOL();
            clienteEdit = new Cliente();
            clienteEdit.id = 0;
            MostrarDatos();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtEmail.Text.Trim()) || String.IsNullOrEmpty(txtNombre.Text.Trim())
                    || String.IsNullOrEmpty(txttelefono.Text.Trim()) || String.IsNullOrEmpty(txtdireccion.Text.Trim()))
                {
                    notificacion.Text = "Datos vacios";
                    notificacion.Visible = true;

                }
                else
                {
                    clienteEdit.nombre = txtNombre.Text.Trim();
                    clienteEdit.email = txtEmail.Text.Trim();
                    clienteEdit.direccion = txtdireccion.Text.Trim();
                    clienteEdit.telefono = txttelefono.Text.Trim();
                    Clientebol.insetarCliente(clienteEdit);
                    MessageBox.Show("Datos agregados");
                    limpieza();
                    MostrarDatos();
                    clienteEdit.id = 0;


                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void limpieza()
        {
            txtNombre.Text = "";
            txtEmail.Text = "";
            txtdireccion.Text = "";
            txttelefono.Text = "";
        }

        private void dgClientes_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int row = dgClientes.SelectedRows.Count > 0 ? dgClientes.SelectedRows[0].Index : -1;
            if (row >= 0)
            {

                clienteEdit = (Cliente)dgClientes.CurrentRow.DataBoundItem;
                MessageBox.Show("Has seleccionado la columna de: " + clienteEdit.nombre);

            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (clienteEdit.id > 0)
            {
                txtNombre.Text = clienteEdit.nombre;
                txtEmail.Text = clienteEdit.email;
                txtdireccion.Text = clienteEdit.direccion;
                txttelefono.Text = clienteEdit.telefono;

            }
            else
            {
                MessageBox.Show("Debe seleccionar un cliente antes de modificarlo");
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (clienteEdit.id > 0)
            {
                DialogResult oDlgRes;

                oDlgRes = MessageBox.Show("¿Está seguro que desea eliminar" +
                    " el registro seleccionado ?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                if (oDlgRes == DialogResult.Yes)
                {
                    Clientebol.Eliminar(clienteEdit);
                    clienteEdit.id = 0;
                    MessageBox.Show("Columna eliminada");
                    MostrarDatos();
                }
                else
                {
                    MessageBox.Show("No se elimino la columna");
                }


            }
            else
            {
                MessageBox.Show("Debe seleccionar un cliente antes de Eliminarlo");
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            List<Cliente> p = Clientebol.CargarClientes(txtBuscar.Text.Trim());
            dgClientes.DataSource = p;
        }

        private void clienteBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (clienteEdit.id > 0)
            {
                FacturasPorCliente mp = new FacturasPorCliente
                {
                    Cliente = clienteEdit
                };
                mp.Show(this);

            }
            else
            {
                MessageBox.Show("Debe seleccionar un cliente antes de modificarlo");
            }
        
        }
    }
}
