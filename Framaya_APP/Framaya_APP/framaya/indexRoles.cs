﻿using Framaya_APP.framayaBOL;
using Framaya_APP.framayaEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Framaya_APP.framaya
{
    public partial class indexRoles : Form
    {
        private RolBOL rolbol;
        private Rol editrol;
        public indexRoles()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtDes.Text.Trim()) || String.IsNullOrEmpty(txtNombre.Text.Trim()))
                {
                    notificacion.Text = "Datos vacios";
                    notificacion.Visible = true;

                }
                else
                {
                    editrol.nombre = txtNombre.Text.Trim();
                    editrol.descripcion = txtDes.Text.Trim();

                    rolbol.insetarRol(editrol);
                    MessageBox.Show("Datos agregados");
                    limpieza();
                    MostrarDatos();
                    editrol.id = 0;


                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void limpieza()
        {
            txtNombre.Text = "";
            txtDes.Text = "";


        }

        private void MostrarDatos()
        {
            List<Rol> p = rolbol.CargarRoles("");

            dgroles.DataSource = p;

        }



        private void indexRoles_Load(object sender, EventArgs e)
        {
            editrol = new Rol();
            editrol.id = 0;
            rolbol = new RolBOL();
            notificacion.Visible = false;
            MostrarDatos();
        }

        private void dgroles_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (editrol.id > 0)
            {
                txtNombre.Text = editrol.nombre;
                txtDes.Text = editrol.descripcion;

            }
            else
            {
                MessageBox.Show("Debe seleccionar un rol antes de modificarlo");
            }

        }

        private void dgroles_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int row = dgroles.SelectedRows.Count > 0 ? dgroles.SelectedRows[0].Index : -1;
            if (row >= 0)
            {
   
                editrol = (Rol)dgroles.CurrentRow.DataBoundItem;
                MessageBox.Show("Has seleccionado la columna de: " + editrol.nombre);

            }

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

            if (editrol.id > 0)
            {
                DialogResult oDlgRes;

                oDlgRes = MessageBox.Show("¿Está seguro que desea eliminar" +
                    " el registro seleccionado ?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                if (oDlgRes == DialogResult.Yes)
                {
                    rolbol.Eliminar(editrol);
                    editrol.id = 0;
                    MessageBox.Show("Columna eliminada");
                    MostrarDatos();
                }
                else
                {
                    MessageBox.Show("No se elimino la columna");
                }
       

                }
            else
            {
                MessageBox.Show("Debe seleccionar un producto antes de Eliminarlo");
            }


            
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            List<Rol> p = rolbol.CargarRoles(txtBuscar.Text.Trim());
            dgroles.DataSource = p;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
