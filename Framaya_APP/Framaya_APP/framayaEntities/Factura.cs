﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framaya_APP.framayaEntities
{
    public class Factura
    {
        public int id { get; set; }
        public DateTime Fecha { get; set; }
        public Decimal monto { get; set; }
        public string estado { get; set; }
        public Usuario usuario { get; set; }
        public string NombreUsuario { get; set; }
        public Prestamista prestamista { get; set; }
        public string NombrePrestamista { get; set; }
        public Empresa empresa { get; set; }
        public string NombreEmpresa { get; set; }
        public Cliente cliente { get; set; }
        public string NombreCliente { get; set; }
        public int active { get; set; }

    }
}
