﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framaya_APP.framayaEntities
{
    public class Usuario
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string cedula { get; set; }
        public string telefono { get; set; }
        public string direccion { get; set; }
        public string pass { get; set; }
        public Rol rol { get; set; }
        public string rolname { get; set; }
        public bool active { get; set; }

    }
}
