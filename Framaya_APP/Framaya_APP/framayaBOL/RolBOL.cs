﻿using Framaya_APP.framayaDAL;
using Framaya_APP.framayaEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framaya_APP.framayaBOL
{
    class RolBOL
    {
        RolesDAL roldal;

        public RolBOL()
        {
            this.roldal = new RolesDAL();
        }

        public void insetarRol(Rol rol)
        {
            if (String.IsNullOrEmpty(rol.descripcion) || String.IsNullOrEmpty(rol.nombre))
            {
                throw new Exception("Datos requeridos");

            }
            if(rol.id > 0)
            {
                roldal.ModificarRol(rol);
            }
            else
            {
                roldal.Insert(rol);
            }
        }

        public void Eliminar(Rol rol)
        {
            roldal.EliminarRol(rol);
        }

        public List<Rol> CargarRoles(string filtro)
        {
            return roldal.CargarDatos(filtro);
        }

    }
}
