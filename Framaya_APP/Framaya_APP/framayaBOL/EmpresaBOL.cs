﻿using Framaya_APP.framayaDAL;
using Framaya_APP.framayaEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framaya_APP.framayaBOL
{
    class EmpresaBOL
    {
        EmpresaDAL dal;

        public EmpresaBOL()
        {
            this.dal = new EmpresaDAL();
        }

        public List<Empresa> Cargar(string filtro)
        {
            return dal.CargarDatos(filtro);
        }

        public void insetar(Empresa user)
        {
            if (String.IsNullOrEmpty(user.email) || String.IsNullOrEmpty(user.nombre) || String.IsNullOrEmpty(user.telefono)
                || String.IsNullOrEmpty(user.direccion) || String.IsNullOrEmpty(user.Tipo_Empresa))
            {
                throw new Exception("Datos requeridos");

            }
            if (user.id > 0)
            {
                dal.Modificar(user);
            }
            else
            {
                dal.Insert(user);
            }
        }

        public void Eliminar(Empresa user)
        {
            dal.Eliminar(user);
        }
    }
}
