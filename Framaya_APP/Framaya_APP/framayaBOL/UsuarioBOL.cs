﻿using Framaya_APP.framayaDAL;
using Framaya_APP.framayaEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framaya_APP.framayaBOL
{
    class UsuarioBOL
    {
        UsuarioDAL usuariodal;

        public UsuarioBOL()
        {
            this.usuariodal = new UsuarioDAL();
        }

        public List<Usuario> CargarUsuarios(string filtro)
        {
            return usuariodal.CargarDatos(filtro);
        }

        public void Eliminar(Usuario rol)
        {
            usuariodal.Eliminar(rol);
        }

        public void insetarUsurario(Usuario user)
        {
            if (String.IsNullOrEmpty(user.cedula) || String.IsNullOrEmpty(user.nombre) || String.IsNullOrEmpty(user.telefono)
                || String.IsNullOrEmpty(user.pass))
            {
                throw new Exception("Datos requeridos");

            }
            if (user.id > 0)
            {
                usuariodal.ModificarUsuario(user);
            }
            else
            {
                usuariodal.Insert(user);
            }
        }
    }
}
