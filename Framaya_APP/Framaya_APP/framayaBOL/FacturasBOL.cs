﻿using Framaya_APP.framayaDAL;
using Framaya_APP.framayaEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framaya_APP.framayaBOL
{
    class FacturasBOL
    {
        FacturaDAL dal;

        public FacturasBOL()
        {
            this.dal = new FacturaDAL();
        }

        public List<Factura> Cargar(string filtro)
        {
            return dal.CargarDatos(filtro);
        }

        public List<Factura> facutaxCliente(string filtro)
        {
            return dal.FacturaPorCliente(filtro);
        }

        public void insetar(Factura f)
        {
            
                dal.Insert(f);
            
        }

        public void Eliminar(Factura f)
        {
            dal.Eliminar(f);
        }
    }
}
