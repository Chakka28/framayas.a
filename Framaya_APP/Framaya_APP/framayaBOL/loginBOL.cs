﻿using Framaya_APP.framayaDAL;
using Framaya_APP.framayaEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framaya_APP.framayaBOL
{
    public class loginBOL
    {
        LoginDAL login;


        public loginBOL()
        {
            this.login = new LoginDAL();
        }

        public Usuario Login(Usuario user)
        {
            if (String.IsNullOrEmpty(user.cedula) || String.IsNullOrEmpty(user.pass))
            {
                throw new Exception("Datos requeridos");

            }
            else
            {
                return login.Login(user);
            }
        }


    }
}
