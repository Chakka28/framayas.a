﻿using Framaya_APP.framayaDAL;
using Framaya_APP.framayaEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framaya_APP.framayaBOL
{
    class PrestamistaBOL
    {
        PrestamistaDAL Prestamistadal;

        public PrestamistaBOL()
        {
            this.Prestamistadal = new PrestamistaDAL();
        }

        public List<Prestamista> Cargarpretamistas(string filtro)
        {
            return Prestamistadal.CargarDatos(filtro);
        }

        public void insetarPrestamista(Prestamista user)
        {
            if (String.IsNullOrEmpty(user.email) || String.IsNullOrEmpty(user.nombre) || String.IsNullOrEmpty(user.telefono)
                || String.IsNullOrEmpty(user.direccion))
            {
                throw new Exception("Datos requeridos");

            }
            if (user.id > 0)
            {
                Prestamistadal.ModificarPrestamista(user);
            }
            else
            {
                Prestamistadal.Insert(user);
            }
        }

        public void Eliminar(Prestamista user)
        {
            Prestamistadal.Eliminar(user);
        }

    }
}

