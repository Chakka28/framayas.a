﻿using Framaya_APP.framayaDAL;
using Framaya_APP.framayaEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framaya_APP.framayaBOL
{
    class ClienteBOL
    {
        ClientesDAL Clientedal;

        public ClienteBOL()
        {
            this.Clientedal = new ClientesDAL();
        }

        public List<Cliente> CargarClientes(string filtro)
        {
            return Clientedal.CargarDatos(filtro);
        }

        public void insetarCliente(Cliente user)
        {
            if (String.IsNullOrEmpty(user.email) || String.IsNullOrEmpty(user.nombre) || String.IsNullOrEmpty(user.telefono)
                || String.IsNullOrEmpty(user.direccion))
            {
                throw new Exception("Datos requeridos");

            }
            if (user.id > 0)
            {
                Clientedal.ModificarCliente(user);
            }
            else
            {
                Clientedal.Insert(user);
            }
        }

        public void Eliminar(Cliente user)
        {
            Clientedal.Eliminar(user);
        }

    }
}
