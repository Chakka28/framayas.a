﻿using Framaya_APP.framayaEntities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framaya_APP.framayaDAL
{
    class UsuarioDAL
    {
        private SqlCommand cmd;
        private SqlDataReader dr;

        private Usuario CrearUsuario(SqlDataReader dr)
        {
            Usuario user = new Usuario();
            user.id = dr.GetInt32(0);
            user.cedula = dr.GetString(1);
            user.nombre = dr.GetString(2);
            user.telefono = dr.GetString(3);
            user.direccion = dr.GetString(4);
            user.pass = dr.GetString(6);
            user.rolname = dr.GetString(9);
            user.active = true;
            user.rol = new Rol();
            user.rol.id = dr.GetInt32(8);
            user.rol.nombre = dr.GetString(9);
            user.rol.descripcion = dr.GetString(10);

            return user;
        }

        public List<Usuario> CargarDatos(string filtro)
        {
            List<Usuario> roles = new List<Usuario>();
            conexion con = new conexion();
            con.open();
            try
            {
                String sql = "SELECT * FROM Usuarios INNER JOIN Roles ON Usuarios.Id_Rol = Roles.id " +
                    " where (Usuarios.NombreCompleto like '" + filtro + "%' or  Roles.Nombre like '" + filtro + "%' )" +
                    "and Usuarios.activo = 1";

                cmd = new SqlCommand(sql, con.con);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    roles.Add(CrearUsuario(dr));

                }
                con.Close();
                return roles;


            }
            catch (Exception)
            {

                throw;
            }

        }

        public bool Insert(Usuario user)
        {
            conexion con = new conexion();
            con.open();
            int numero = Convert.ToInt32(user.rolname);
            try
            {
                cmd = new SqlCommand("insert into Usuarios(Cedula,NombreCompleto,Telefono,Direccion,pass, id_Rol,activo) values" +
                    " ('" + user.cedula + "','" + user.nombre + "','" + user.telefono + "','" + user.direccion + "','" + user.pass + "'," + numero + ",1)", con.con);
                cmd.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch
            {
                throw new Exception("Error al registrar el Usuario");
            }



        }
        public bool ModificarUsuario(Usuario user)
        {
            conexion con = new conexion();
            con.open();
            int numero = Convert.ToInt32(user.rolname);
            try
            {
                String sql = "UPDATE Usuarios SET NombreCompleto = @nombre, Cedula = @cedula, Pass = @pass, Telefono = @telefono , Direccion = @direccion, id_Rol = @id_Rol where  id = @id";
                cmd = new SqlCommand(sql, con.con);
                cmd.Parameters.AddWithValue("@nombre", user.nombre);
                cmd.Parameters.AddWithValue("@cedula", user.cedula);
                cmd.Parameters.AddWithValue("@pass", user.pass);
                cmd.Parameters.AddWithValue("@telefono", user.telefono);
                cmd.Parameters.AddWithValue("@direccion", user.direccion);
                cmd.Parameters.AddWithValue("@id_Rol", numero);
                cmd.Parameters.AddWithValue("@id",user.id);
                cmd.ExecuteNonQuery();
                return true;

            }
            catch (Exception)
            {

                throw new Exception("Error a la hora de realizar la actualización de datos");
            }




        }

        public bool Eliminar(Usuario user)
        {
            conexion con = new conexion();
            con.open();
            try
            {
                String sql = "UPDATE Usuarios SET activo = @activo where id = @id";
                cmd = new SqlCommand(sql, con.con);
                cmd.Parameters.AddWithValue("@activo", 2);
                cmd.Parameters.AddWithValue("@id", user.id);
                cmd.ExecuteNonQuery();
                return true;

            }
            catch (Exception)
            {

                throw new Exception("Error a la hora de realizar la actualización de datos");
            }

        }


    }
}
