﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framaya_APP.framayaDAL
{
    class conexion
    {
        public SqlConnection con = new SqlConnection("Data Source =  LAPTOP-F9Q2F3TA\\SQLEXPRESS;Initial Catalog=FramayaSA;Integrated Security =True");
        public conexion()
        {

        }

        public SqlConnection cone()
        {
            return con;
        }

        /// <summary>
        /// Open de data base 
        /// </summary>
        public void open()
        {

            try
            {
                con.Open();

            }
            catch
            {
                Console.WriteLine("Error al abrir la base de datos");
            }
        }

        /// <summary>
        /// Close de data base 
        /// </summary>
        public void Close()
        {
            con.Close();
        }
    }
}

