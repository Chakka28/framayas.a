﻿using Framaya_APP.framayaEntities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framaya_APP.framayaDAL
{
    class ClientesDAL
    {
        private SqlCommand cmd;
        private SqlDataReader dr;


        public List<Cliente> CargarDatos(string filtro)
        {
            List<Cliente> clientes = new List<Cliente>();
            conexion con = new conexion();
            con.open();
            try
            {
                String sql = "SELECT * FROM Cliente" +
                    " where (Nombre like '" + filtro + "%' or Email like '" + filtro + "%' )" +
                    "and Activo = 1";

                cmd = new SqlCommand(sql, con.con);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    clientes.Add(CrearCliente(dr));

                }
                con.Close();
                return clientes;


            }
            catch (Exception)
            {

                throw;
            }

        }

        private Cliente CrearCliente(SqlDataReader dr)
        {
            Cliente user = new Cliente();
            user.id = dr.GetInt32(0);
            user.nombre = dr.GetString(1);
            user.email = dr.GetString(2);
            user.telefono = dr.GetString(3);
            user.direccion = dr.GetString(4);
            user.active = dr.GetInt32(5);

            return user;
        }

        public bool Insert(Cliente user)
        {
            conexion con = new conexion();
            con.open();
            try
            {
                cmd = new SqlCommand("insert into Cliente(Nombre,Email,Telefono,Direccion,Activo) values" +
                    " ('" + user.nombre + "','" + user.email + "','" + user.telefono + "','" + user.direccion + "',1)", con.con);
                cmd.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch
            {
                throw new Exception("Error al registrar el cliente");
            }
        }

        public bool ModificarCliente(Cliente user)
        {
            conexion con = new conexion();
            con.open();
            try
            {
                String sql = "UPDATE Cliente SET Nombre = @nombre, Email = @email,Telefono = @telefono , Direccion = @direccion where  id = @id";
                cmd = new SqlCommand(sql, con.con);
                cmd.Parameters.AddWithValue("@nombre", user.nombre);
                cmd.Parameters.AddWithValue("@email", user.email);
                cmd.Parameters.AddWithValue("@telefono", user.telefono);
                cmd.Parameters.AddWithValue("@direccion", user.direccion);
                cmd.Parameters.AddWithValue("@id", user.id);
                cmd.ExecuteNonQuery();
                return true;

            }
            catch (Exception)
            {

                throw new Exception("Error a la hora de realizar la actualización de datos");
            }
        }

        public bool Eliminar(Cliente user)
        {
            conexion con = new conexion();
            con.open();
            try
            {
                String sql = "UPDATE Cliente SET activo = @activo where id = @id";
                cmd = new SqlCommand(sql, con.con);
                cmd.Parameters.AddWithValue("@activo", 2);
                cmd.Parameters.AddWithValue("@id", user.id);
                cmd.ExecuteNonQuery();
                return true;

            }
            catch (Exception)
            {

                throw new Exception("Error a la hora de realizar la actualización de datos");
            }

        }


    }


}
