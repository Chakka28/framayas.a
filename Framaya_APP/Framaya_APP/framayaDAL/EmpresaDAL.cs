﻿using Framaya_APP.framayaEntities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framaya_APP.framayaDAL
{
    class EmpresaDAL
    {
        private SqlCommand cmd;
        private SqlDataReader dr;


        public List<Empresa> CargarDatos(string filtro)
        {
            List<Empresa> empresa = new List<Empresa>();
            conexion con = new conexion();
            con.open();
            try
            {
                String sql = "SELECT * FROM Empresa" +
                    " where (Nombre like '" + filtro + "%' or TipoEmpresa like '" + filtro + "%' )" +
                    "and Activo = 1";

                cmd = new SqlCommand(sql, con.con);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    empresa.Add(Crear(dr));

                }
                con.Close();
                return empresa;


            }
            catch (Exception)
            {

                throw;
            }

        }

        private Empresa Crear(SqlDataReader dr)
        {
            Empresa user = new Empresa();
            user.id = dr.GetInt32(0);
            user.Tipo_Empresa = dr.GetString(1);
            user.nombre = dr.GetString(2);
            user.email = dr.GetString(3);
            user.telefono = dr.GetString(4);
            user.direccion = dr.GetString(5);
            user.active = dr.GetInt32(6);

            return user;
        }


        public bool Insert(Empresa user)
        {
            conexion con = new conexion();
            con.open();
            try
            {
                cmd = new SqlCommand("insert into Empresa(Nombre,Email,Telefono,TipoEmpresa,Direccion,activo) values" +
                    " ('" + user.nombre + "','" + user.email + "','" + user.telefono + "','" + user.Tipo_Empresa + "','" + user.direccion + "',1)", con.con);
                cmd.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch
            {
                throw new Exception("Error al registrar el Empresa");
            }
        }

        public bool Modificar(Empresa user)
        {
            conexion con = new conexion();
            con.open();
            try
            {
                String sql = "UPDATE Empresa SET Nombre = @nombre, Email = @email,Telefono = @telefono , Direccion = @direccion, TipoEmpresa = @tipo where  id = @id";
                cmd = new SqlCommand(sql, con.con);
                cmd.Parameters.AddWithValue("@nombre", user.nombre);
                cmd.Parameters.AddWithValue("@email", user.email);
                cmd.Parameters.AddWithValue("@telefono", user.telefono);
                cmd.Parameters.AddWithValue("@direccion", user.direccion);
                cmd.Parameters.AddWithValue("@tipo", user.Tipo_Empresa);
                cmd.Parameters.AddWithValue("@id", user.id);
                cmd.ExecuteNonQuery();
                return true;

            }
            catch (Exception)
            {

                throw new Exception("Error a la hora de realizar la actualización de datos");
            }
        }


        public bool Eliminar(Empresa user)
        {
            conexion con = new conexion();
            con.open();
            try
            {
                String sql = "UPDATE Empresa SET Activo = @activo where id = @id";
                cmd = new SqlCommand(sql, con.con);
                cmd.Parameters.AddWithValue("@activo", 2);
                cmd.Parameters.AddWithValue("@id", user.id);
                cmd.ExecuteNonQuery();
                return true;

            }
            catch (Exception)
            {

                throw new Exception("Error a la hora de realizar la actualización de datos");
            }

        }


    }
}
