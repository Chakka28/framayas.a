﻿using Framaya_APP.framayaEntities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framaya_APP.framayaDAL
{
    class FacturaDAL
    {
        private SqlCommand cmd;
        private SqlDataReader dr;

        private Factura Crear(SqlDataReader dr)
        {
            Factura factura = new Factura();
            try
            {
                
                factura.id = dr.GetInt32(0);
                factura.Fecha = dr.GetDateTime(1);
                factura.monto = dr.GetDecimal(2);
                factura.estado = dr.GetString(4);
                factura.active = dr.GetInt32(8);
                factura.NombreCliente = dr.GetString(18);
               factura.NombreUsuario = dr.GetString(11);
               factura.NombreEmpresa = dr.GetString(25);
               factura.NombrePrestamista = dr.GetString(31);
               factura.usuario = new Usuario();
               factura.usuario.id = dr.GetInt32(9);
               factura.cliente = new Cliente();
               factura.cliente.id = dr.GetInt32(17);
               factura.cliente.email = dr.GetString(19);
               factura.empresa = new Empresa();
               factura.empresa.id = dr.GetInt32(23);
               factura.empresa.email = dr.GetString(26);
               factura.prestamista = new Prestamista();
               factura.prestamista.id = dr.GetInt32(30);
               factura.prestamista.email = dr.GetString(32);
            }
            catch (Exception e)
            {

                throw e;
            }
            
            return factura;
        }

        public List<Factura> CargarDatos(string filtro)
        {
            List<Factura> facturas = new List<Factura>();
            conexion con = new conexion();
            con.open();
            try
            {
                String sql = "SELECT * FROM Facturas INNER JOIN Usuarios ON Facturas.id_Usuario = Usuarios.ID INNER JOIN Cliente ON Facturas.id_Cliente = Cliente.ID INNER JOIN Empresa ON Facturas.Id_Empresa = Empresa.ID INNER JOIN Prestamista ON Facturas.id_Prestamista = Prestamista.ID" +
                    " where(Facturas.Estado like '" + filtro + "%' or  Empresa.Nombre like '" + filtro +"%' or  Cliente.Nombre like '" + filtro + "%')" +
                    " and Facturas.Activo = 1";

                cmd = new SqlCommand(sql, con.con);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    facturas.Add(Crear(dr));

                }
                con.Close();
                return facturas;


            }
            catch (Exception e)
            {

                throw e;
            }



        }

        public List<Factura> FacturaPorCliente(string filtro)
        {
            List<Factura> facturas = new List<Factura>();
            conexion con = new conexion();
            con.open();
            try
            {
                String sql = "SELECT * FROM Facturas INNER JOIN Usuarios ON Facturas.id_Usuario = Usuarios.ID INNER JOIN Cliente ON Facturas.id_Cliente = Cliente.ID INNER JOIN Empresa ON Facturas.Id_Empresa = Empresa.ID INNER JOIN Prestamista ON Facturas.id_Prestamista = Prestamista.ID" +
                    " where Facturas.id_Cliente =" + filtro + 
                    " and Facturas.Activo = 1";

                cmd = new SqlCommand(sql, con.con);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    facturas.Add(Crear(dr));

                }
                con.Close();
                return facturas;


            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public bool Insert(Factura f)
        {
            conexion con = new conexion();
            con.open();
            try
            {
                cmd = new SqlCommand("insert into Facturas(Fecha,Monto,Id_Empresa,Estado,id_Usuario,id_Cliente,id_Prestamista, Activo) values" +
                    " ('" + f.Fecha.ToShortDateString() + "'," + f.monto + "," + f.NombreEmpresa + ",'" + f.estado + "'," + f.NombreUsuario + "," + f.NombreCliente + "," + f.NombrePrestamista + ",1)", con.con);
                cmd.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch
            {
                throw new Exception("Error al registrar el factura");
            }

     }

        public bool Eliminar(Factura factura)
        {
            conexion con = new conexion();
            con.open();
            try
            {
                String sql = "UPDATE Facturas SET Activo = @activo where id = @id";
                cmd = new SqlCommand(sql, con.con);
                cmd.Parameters.AddWithValue("@activo", 2);
                cmd.Parameters.AddWithValue("@id", factura.id);
                cmd.ExecuteNonQuery();
                return true;

            }
            catch (Exception)
            {

                throw new Exception("Error a la hora de realizar la actualización de datos");
            }

        }
    }
}
