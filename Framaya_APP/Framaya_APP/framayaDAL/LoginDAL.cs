﻿using Framaya_APP.framayaEntities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framaya_APP.framayaDAL
{
    class LoginDAL
    {
        private SqlCommand cmd;
        private SqlDataReader dr;


        public Usuario Login(Usuario user)
        {
            conexion con = new conexion();
            con.open();

            try
            {
                cmd = new SqlCommand("SELECT * FROM Usuarios  INNER JOIN Roles ON Usuarios.Id_Rol = Roles.id  where" +
                    " Usuarios.Cedula = '" + user.cedula + "' and pass= '" + user.pass + "'", con.con);
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    user.id = dr.GetInt32(0);
                    user.cedula = dr.GetString(1);
                    user.nombre = dr.GetString(2);
                    user.telefono = dr.GetString(3);
                    user.direccion = dr.GetString(4);
                    user.pass = dr.GetString(6);
                    user.active = true;
                    user.rol = new Rol();
                    user.rol.id = dr.GetInt32(8);
                    user.rol.nombre = dr.GetString(9);
                    user.rol.descripcion = dr.GetString(10);

                }

                con.Close();

            }
            catch
            {
                Console.WriteLine("Error con la base de datos");
            }
            con.Close();

            return user;
        }


    }
}
