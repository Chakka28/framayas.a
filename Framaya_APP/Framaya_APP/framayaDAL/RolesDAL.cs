﻿using Framaya_APP.framayaEntities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framaya_APP.framayaDAL
{
    class RolesDAL
    {
        private SqlCommand cmd;
        private SqlDataReader dr;

        public bool Insert(Rol rol)
        {
            conexion con = new conexion();
            con.open();

            try
            {
                cmd = new SqlCommand("insert into Roles(Nombre,Descripcion, activo) values ('" + rol.nombre + "','" + rol.descripcion + "', 1)", con.con);
                cmd.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch
            {
                throw new Exception("Error al registrar el rol");
            }



        }

        public List<Rol> CargarDatos(string filtro)
        {
            List<Rol> roles = new List<Rol>();
            conexion con = new conexion();
            con.open();
            try
            {
                String sql = "select * from Roles where (Nombre like '" + filtro + "%' or  Descripcion like '" + filtro + "%' )and activo = 1";

                cmd = new SqlCommand(sql, con.con);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    roles.Add(CrearRol(dr));

                }
                con.Close();
                return roles;
                

            }
            catch (Exception)
            {

                throw;
            }

        }

        private Rol CrearRol(SqlDataReader dr)
        {
            Rol p = new Rol();
            p.id = dr.GetInt32(0);
            p.nombre = dr.GetString(1);
            p.descripcion = dr.GetString(2);
            
            return p;
        }


        public bool ModificarRol(Rol rol)
        {
            conexion con = new conexion();
            con.open();
            try
            {
                String sql = "UPDATE Roles SET Nombre = @nombre, Descripcion = @descripcion where  id = @id";
                cmd = new SqlCommand(sql, con.con);
                cmd.Parameters.AddWithValue("@nombre", rol.nombre);
                cmd.Parameters.AddWithValue("@descripcion", rol.descripcion);
                cmd.Parameters.AddWithValue("@id", rol.id);
                cmd.ExecuteNonQuery();
                return true;

            }
            catch (Exception)
            {

                throw new Exception("Error a la hora de realizar la actualización de datos");
            }




        }

        public bool EliminarRol(Rol rol)
        {
            conexion con = new conexion();
            con.open();
            try
            {
                String sql = "UPDATE Roles SET activo = @activo where id = @id";
                cmd = new SqlCommand(sql, con.con);
                cmd.Parameters.AddWithValue("@activo", 2);
                cmd.Parameters.AddWithValue("@id", rol.id);
                cmd.ExecuteNonQuery();
                return true;

            }
            catch (Exception)
            {

                throw new Exception("Error a la hora de realizar la actualización de datos");
            }

        }




    }
}
